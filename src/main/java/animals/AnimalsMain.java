package animals;

import java.util.Scanner;

import animals.model.Animal;

/**
 * @author Przemysław Ziegert
 */
public class AnimalsMain {

    public static void main(String[] args) {

        Animal[] animals = new Animal[10];

        /* Variable to set how long the infinity loop will be running. */
        boolean running = true;


        // Loop
        while (running) {
            /* System.in has no convenient methods for reading input so we will use scanner. */
            Scanner scanner = new Scanner(System.in);
            /* Read next user command. */
            int command = scanner.nextInt();

            switch (command) {
                case 1: {
                    // display index array
                    int index = scanner.nextInt();
                    // Display our variables out
                    String name = scanner.next();
                    int age = scanner.nextInt();


                    /* Add new animal */
                    Animal animal = new Animal();
                    animal.setName(name);
                    animal.setAge(age);


                    animals[index] = animal;
                break;

                }
                case 2: {

                    int index = scanner.nextInt();
                    // Print name and age
                    for (Animal animal : animals) {
                        if (animal != null) {
                            System.out.println("NAME: " + animals[index].getName());
                            System.out.println("AGE: " + animals[index].getAge());
                            break;
                        }
                    }
                }
                case 0: {
                    running = false;
                    break;
                }
                default: {
                    System.out.println("Valid commands are:\n" +
                            "1 - add,\n" +
                            "2 - print,\n" +
                            "0 - quit.\n");
                }
            }
        }
    }

}